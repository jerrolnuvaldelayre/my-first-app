package com.example.jerrol.myfirstapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText

const val EXTRA_FIRSTNAME = "com.example.jerrol.myfirstapplication.FIRSTNAME"
const val EXTRA_LASTNAME = "com.example.jerrol.myfirstapplication.LASTNAME"
const val EXTRA_MESSAGE = "com.example.jerrol.myfirstapplication.MESSAGE"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun sendMessagge(view: View) {
        val editLastName = findViewById<EditText>(R.id.edit_lastname)
        val editFirstName = findViewById<EditText>(R.id.edit_firstname)
        val editMessage = findViewById<EditText>(R.id.edit_message)

        val intent = Intent(this, DisplayMessageActivity::class.java).apply {
            putExtra(EXTRA_FIRSTNAME, editFirstName.text.toString())
            putExtra(EXTRA_LASTNAME, editLastName.text.toString())
            putExtra(EXTRA_MESSAGE, editMessage.text.toString())
        }

        startActivity(intent)
    }
}
